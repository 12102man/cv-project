import cv2
import os
from PIL import Image
from torch.utils.data import Dataset
from torchvision import datasets, transforms
import numpy as np
import xml.etree.ElementTree as ET

transforms = transforms.Compose([
    transforms.Resize((416, 416)),
    transforms.ToTensor(),
])


class YoloTrainDataset(Dataset):
    def __init__(self, root_dir):
        self.root_dir = root_dir
        self.images_paths = os.listdir(
            os.path.join(root_dir, 'images')
        )
        self.labels_path = os.listdir(
            os.path.join(root_dir, 'annotations')
        )
        self.transforms = transforms

    def __len__(self):
        return len(self.images_paths)

    def __getitem__(self, idx):
        image_name = self.images_paths[idx]
        image_path = os.path.join(
            self.root_dir,
            'images',
            image_name,
        )
        image = Image.fromarray(
            cv2.imread(
                image_path
            )
        )
        tensor_image = self.transforms(image)
        regions = self.unpack_labels(image_name)
        return image_name, tensor_image, regions

    def unpack_labels(self, filename):
        annotation_name = ''.join(filename.split('.')[:-1]) + '.xml'
        annotation_tree = ET.parse(
            os.path.join(
                self.root_dir,
                'annotations',
                annotation_name
            )
        )
        parsed_options = []
        option = annotation_tree.getroot().find('object')
        xmin = int(option.find('bndbox').find('xmin').text)
        xmax = int(option.find('bndbox').find('xmax').text)
        ymin = int(option.find('bndbox').find('ymin').text)
        ymax = int(option.find('bndbox').find('ymax').text)
        parsed_options.append([xmin, xmax, ymin, ymax])
        return np.array(parsed_options)
