from flask import Flask, request, Response, make_response
import numpy as np
import cv2
import json
from detector import detector

app = Flask(__name__)


@app.route('/')
def index():
    return "Hello, World!"


@app.route('/extract_number', methods=['POST'])
def extract_number():
    needPoints = request.form.get('points', 'No')
    print(needPoints)
    if needPoints == 'Yes':
        needPoints = True
    else:
        needPoints = False

    file = request.files['image'].read()
    nparr = np.fromstring(file, np.uint8)
    img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    out_img, contours = detector.extract_contours(img)
    retval, buffer = cv2.imencode('.jpg', out_img)
    if needPoints:
        response = {
            'contours': contours.tolist()
        }
        return Response(
            response=json.dumps(response),
            status=200,
            mimetype="application/json"
        )
    else:
        response = make_response(buffer.tobytes())
        response.headers['Content-Type'] = 'image/png'
        return response

if __name__ == '__main__':
    app.run(debug=True)
