import detector.yolov3.main as yolov3
import cv2

cap = cv2.VideoCapture('record.avi')


frame_width = int(cap.get(3))
frame_height = int(cap.get(4))
out = cv2.VideoWriter('outpy.avi', cv2.VideoWriter_fourcc('M','J','P','G'), 10, (frame_width,frame_height))
yolov3_model = yolov3.init_model()

print('Starting detection...')

while cap.isOpened():
    ret, image = cap.read()
    if ret == True:
        print('all good')
        out_image = image.copy()
        yolov3_points = yolov3.get_contours(
            yolov3_model,
            out_image
        )

        cv2.polylines(out_image, yolov3_points, True, (255, 129, 70), thickness=2)
        out.write(out_image)
        print('processed frame')
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break


cap.release()
out.release()
