## Car plates frame detection using YOLO system

This repository contains the code for Introduction to CV course project. 
Here you can find a Flask API that taken image returns either positions of contours found 
or image with highlighed points.

#### Run
To see project results, please follow these steps:
1. pip install -r requirements.txt
2. python main.py

### Methods
- POST http://localhost:5000/extract_image
                
                
      {
        "image": %your image in .jpg format%,
        "points": "Yes" - if you need points instead image in response
      }
      
      