import numpy as np

from detector.yolov4.keras_loader import Create_Yolo
from detector.yolov4.utils import detect_image


def init_model():
    yolo = Create_Yolo()
    yolo.load_weights("./detector/yolov4/yolov3_custom")  # use keras weights
    return yolo


def get_contours(yolo, input_image):
    figures = detect_image(
        yolo,
        input_image,
    )
    points = []
    for f in figures:
        x, y, x2, y2, _, _ = f
        w = x2 - x
        h = y2 - y
        points.append(np.array([
            [x, y],
            [x + w, y],
            [x + w, y + h],
            [x, y + h],
        ], np.int32))
    return points
