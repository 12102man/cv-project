import cv2
import numpy as np


def read_class_names(classes_src="detector/classes.names"):
    with open(classes_src, 'rt') as f:
        classes = f.read().rstrip('\n').split('\n')
    return classes


def get_output_layers(net):
    layer_names = net.getLayerNames()
    return [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]


def postprocess(frame, outs):
    confidence_threshold = 0.5
    nms_threshold = 0.4
    frame_height = frame.shape[0]
    frame_width = frame.shape[1]

    class_ids = []
    confidences = []
    boxes = []
    for out in outs:
        for detection in out:
            scores = detection[5:]
            classId = np.argmax(scores)
            confidence = scores[classId]
            if confidence > confidence_threshold:
                center_x = int(detection[0] * frame_width)
                center_y = int(detection[1] * frame_height)
                width = int(detection[2] * frame_width)
                height = int(detection[3] * frame_height)
                left = int(center_x - width / 2)
                top = int(center_y - height / 2)
                class_ids.append(classId)
                confidences.append(float(confidence))
                boxes.append([left, top, width, height])

    indices = cv2.dnn.NMSBoxes(
        boxes,
        confidences,
        confidence_threshold,
        nms_threshold
    )
    coords = []
    for i in indices:
        i = i[0]
        box = boxes[i]
        left = box[0]
        top = box[1]
        width = box[2]
        height = box[3]
        coords.append((left, top, width, height))
    return coords


def init_model(
        model_configuration="detector/yolov3/darknet-yolov3.cfg",
        model_weights="detector/yolov3/lapi.weights"
):
    net = cv2.dnn.readNetFromDarknet(model_configuration, model_weights)
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)
    return net


def get_contours(model, input_image):
    blob = cv2.dnn.blobFromImage(
        input_image,
        1 / 255,
        (416, 416),
        [0, 0, 0],
        1,
        crop=False
    )

    model.setInput(blob)
    outs = model.forward(get_output_layers(model))
    figures = postprocess(input_image, outs)

    points = []
    for f in figures:
        x, y, w, h = f
        points.append(np.array([
            [x, y],
            [x + w, y],
            [x + w, y + h],
            [x, y + h],
        ]))
    return points
