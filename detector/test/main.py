import glob
import os
import xml.etree.ElementTree as ET

import cv2
import numpy as np
import time

import detector.yolov3.main as yolov3
import detector.yolov4.main as yolov4


def parse_data(path='detector/test/images'):
    out = []

    # Part 1. Parse XML data
    for xml_file in glob.glob(path + '/*.xml'):
        tree = ET.parse(open(xml_file))
        root = tree.getroot()

        image_name = root.find('filename').text
        img_path = path + '/' + image_name
        image = cv2.imread(img_path, cv2.IMREAD_COLOR)

        contours = []
        for i, obj in enumerate(root.iter('object')):
            xmlbox = obj.find('bndbox')
            xmin, xmax, ymin, ymax = int(xmlbox.find('xmin').text), \
                                     int(xmlbox.find('xmax').text), \
                                     int(xmlbox.find('ymin').text), \
                                     int(xmlbox.find('ymax').text)
            width, height = xmax - xmin, ymax - ymin

            contour = [
                [xmin, ymin],
                [xmin + width, ymin],
                [xmin + width, ymin + height],
                [xmin, ymin + height]
            ]
            contours.append(np.array(contour, np.int32))
        out.append({
            'name': image_name,
            'image': image,
            'contours': contours
        })
    return out


def iou(contour1, contour2):
    boxA = [
        contour1[0][0],
        contour1[0][1],
        contour1[2][0],
        contour1[2][1]
    ]
    boxB = [
        contour2[0][0],
        contour2[0][1],
        contour2[2][0],
        contour2[2][1]
    ]

    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    iou = interArea / float(boxAArea + boxBArea - interArea)
    return iou


def precision_recall_f1(model, get_contours):
    """
    Metrics calculation
    Source: https://supervise.ly/explore/plugins/precision-and-recall-75278/overview
    :param get_contours: Method for extracting contours
    :param model: model
    :return: precision, recall, f1
    """
    images = parse_data()
    tp = 0
    fp = 0
    tn = 0
    total_time = 0
    for image in images:
        s = time.time()
        predictions = get_contours(
            model,
            image['image']
        )
        e = time.time()
        total_time += (e-s)
        for i in range(len(image['contours'])):
            if len(predictions) <= i:
                tn += 1
            else:
                iou_score = iou(predictions[i], image['contours'][i])
                if iou_score > 0.75:
                    tp += 1
                else:
                    fp += 1
    precision = tp / (tp + fp)
    recall = tp / (tp + tn)
    f1 = 2 * precision * recall / (precision + recall)

    return precision, recall, f1, total_time, total_time / len(images)


if __name__ == '__main__':
    os.chdir("..")
    os.chdir("..")
    # YOLOv3
    yolov3_model = yolov3.init_model()
    print('V3 weights loaded')

    # YOLOv4
    yolov4_model = yolov4.init_model()
    print('V4 weights loaded')

    print(
        precision_recall_f1(yolov3_model, yolov3.get_contours)
    )
    print(
        precision_recall_f1(yolov4_model, yolov4.get_contours)
    )
