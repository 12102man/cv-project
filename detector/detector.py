import cv2
import numpy as np

import detector.yolov3.main as yolov3
import detector.yolov4.main as yolov4


def extract_contours(input_image):
    # YOLOv3
    yolov3_model = yolov3.init_model()
    yolov3_points = yolov3.get_contours(
        yolov3_model,
        input_image
    )

    # YOLOv4
    # yolov4_model = yolov4.init_model()
    # yolov4_points = yolov4.get_contours(
    #     yolov4_model,
    #     input_image
    # )

    cv2.polylines(input_image, yolov3_points, True, (255, 129, 70), thickness=2)
    return input_image, np.array(yolov3_points)
